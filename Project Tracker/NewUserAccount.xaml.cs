﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TrackerDAL;
namespace ProjectTracker
{
    /// <summary>
    /// Interaction logic for NewUserAccount.xaml
    /// </summary>
    public partial class NewUserAccount : Window
    {
        private readonly TrackerContext _dbCtx;
        public NewUserAccount()
        {
            InitializeComponent();
            _dbCtx = new TrackerContext();
        }

        
        private void Sign_Up_Btn_Click(object sender, RoutedEventArgs e)
        {
            using (var dbCtxTransaction = _dbCtx.Database.BeginTransaction())
            {
                try
                {
                    //  add a new account 
                    Account newAcc;
                    newAcc = _dbCtx.Accounts.Add(new Account { AccountName = "Untitled" });
                    _dbCtx.SaveChanges();

                    //check all users information first           
                    User user = new User();
                    user.Firstname = fNameTB.Text;
                    user.Lastname = lNameTB.Text;
                    user.Email = emailTB.Text;
                    user.Username = displayNameTB.Text;

                    if (password1TB.Password == password2TB.Password)
                    {
                        user.Password = password2TB.Password;
                    }
                    else
                    {
                        MessageBox.Show("Passwords do not match!");
                        return;
                    }
                  

                    user = _dbCtx.Users.Add(user);
                    _dbCtx.SaveChanges();

                    //  Add into Users_Accounts table
                    _dbCtx.Users_Accounts.Add(new Users_Accounts() { AccountID = newAcc.AccountID, UserID = user.UserID, AccountRoleID = (int)AccountRole.Role.OWNER });

                    int count = _dbCtx.SaveChanges();

                    dbCtxTransaction.Commit();
                    if (count == 0)
                    {
                        MessageBox.Show("Error: Information not saved! Please try again!");
                        return;
                    }
                    else
                    {
                        //  open dashboard
                        Dashboard dashboard = new Dashboard(user);
                        this.Close();
                        dashboard.ShowDialog();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    dbCtxTransaction.Rollback();
                    return;
                }
            }
           
        }

        //cancel button method that will close the application
        private void Cancel_Btn_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Hitting \"OK\" will cause the application to close.\n" +
                                         "All progress will be lost.",
                                         "ATN & Co. Project Tracker",
                                         MessageBoxButton.OKCancel,
                                         MessageBoxImage.Warning);

            if (result == MessageBoxResult.OK)
            {
                Environment.Exit(0);
            }
            else
            {
                this.Focus();
            }

        }
    }
}
