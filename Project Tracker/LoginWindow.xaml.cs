﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TrackerDAL;

namespace ProjectTracker
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private readonly TrackerContext _dbCtx;
        private User currentUser;
        public LoginWindow()
        {
            InitializeComponent();
            _dbCtx = new TrackerContext();
        }

        //login button that will determine if they have provided correct crdentials and if so,
        //  take them to their dashboard with their current projects and/or make new ones.
        private void Login_Btn_Click(object sender, RoutedEventArgs e)
        {
            //Authenticate the users information here

            bool creds = CheckCredentials(loginEmailTB.Text, loginPasswordTB.Password);

            //if passes, then opens "dashboard" with their projcets and/or let them create new projects
            if (creds == true)
            {
                //  pass user to dashboard 
                Dashboard dashboard = new Dashboard(currentUser);
                this.Close();
                dashboard.ShowDialog();
            }
            //else
            //{
            //    MessageBox.Show("Error: Email and/or password do not match.",
            //                    "ATN & Co. Project Tracker - Login",
            //                     MessageBoxButton.OK,
            //                     MessageBoxImage.Error);
            //}
        }

        //allows a new user to create an account to use the tracker's features.
        private void New_Btn_Click(object sender, RoutedEventArgs e)
        {
            //Opens "NewUserAccount" windows where user can eneter in all their relevent information.
            NewUserAccount newUserAccount = new NewUserAccount();
            this.Close();
            newUserAccount.ShowDialog();
        }

        //Method that checks if the users information is valid.
        // if valid, returns true, else returns false.
        private bool CheckCredentials(string useremail, string userpass)
        {
            try
            {
                //  get user from db by email
                bool exists = _dbCtx.Users.Any(u => u.Email == useremail);
                if(exists) //  if user exists
                {
                    //get user 
                    currentUser = _dbCtx.Users.Where(u => u.Email == useremail).FirstOrDefault();

                    //  check password
                    if (currentUser.Password == userpass)
                    {
                        //  credentials are correct
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Password is incorrect!");
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show("User does not exist!");
                    return false;
                }               
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        //cancel button method that will close the application
        private void Cancel_Btn_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Hitting \"OK\" will cause the application to close.\n" +
                                         "All progress will be lost.",
                                         "ATN & Co. Project Tracker",
                                         MessageBoxButton.OKCancel,
                                         MessageBoxImage.Warning);

            if (result == MessageBoxResult.OK)
            {
                Environment.Exit(0);
            }
            else
            {
                this.Focus();
            }

        }
    }
}
