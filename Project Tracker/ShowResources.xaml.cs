﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TrackerDAL;

namespace ProjectTracker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ShowResources : Window
    {
       
        Account CurrentSelectedAccount;
        TrackerContext _dbCtx;
        ObservableCollection<Resource> people;

        public ShowResources(Account acc)
        {
            _dbCtx = new TrackerContext();
            InitializeComponent();      
            CurrentSelectedAccount = acc;
            this.Loaded += ShowResources_Loaded;

            // to remove extra space from column in the end 
            this.ResourceGrid.AutoGeneratingColumn += dataGrid_AutoGeneratingColumn;

            void dataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
            {
                e.Column.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
            }

        }

       
        private void ShowResources_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //  Get all users associated with this account
                var listOfUsers = _dbCtx.Users_Accounts.Where(u => u.AccountID == CurrentSelectedAccount.AccountID).Select(s=> s.User).ToList();
                people = new ObservableCollection<Resource>();
                foreach (var item in listOfUsers)
                {
                    people.Add(new Resource() { Name = item.Firstname + " " + item.Lastname, Email = item.Email });
                }
                this.DataContext = people;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
          

        }

    }
}
