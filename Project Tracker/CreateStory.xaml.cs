﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TrackerDAL;

namespace ProjectTracker
{
    /// <summary>
    /// Interaction logic for CreateStory.xaml
    /// </summary>
    public partial class CreateStory : Window
    {
        private Project CurrentProject;
        private User CurrentUser;
        private Account CurrentAccount;
        TrackerContext _dbCtx;
        public CreateStory(User user, Account acc, Project proj)
        {
            CurrentUser = user;
            CurrentAccount = acc;
            CurrentProject = proj;
            _dbCtx = new TrackerContext();            
            InitializeComponent();
        }

        /// <summary>
        /// Load Story types
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbStoryType_Loaded(object sender, RoutedEventArgs e)
        {
           cbStoryType.ItemsSource = _dbCtx.StoryTypes.ToList();
        }

        /// <summary>
        /// Loads Priorities into combobox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbStoryPriority_Loaded(object sender, RoutedEventArgs e)
        {
            cbStoryPriority.ItemsSource = _dbCtx.Priorities.ToList();
        }
        /// <summary>
        /// Loads Status into combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbStoryStatus_Loaded(object sender, RoutedEventArgs e)
        {
            cbStoryStatus.ItemsSource = _dbCtx.Status.ToList();            
        }


        /// <summary>
        /// Loads users to set as owner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbOwner_Loaded(object sender, RoutedEventArgs e)
        {
            //  get users in this project 
           var listOfUsers = _dbCtx.Users_Projects_ProjectRoles.Where(u => (u.AccountID == CurrentAccount.AccountID && u.ProjectID == CurrentProject.ProjectID)).Select(s => s.User).ToList();
            cbOwner.ItemsSource = listOfUsers;
        }

        /// <summary>
        /// Loads users to set as requester
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbRequester_Loaded(object sender, RoutedEventArgs e)
        {
            //  get users in this project 
            var listOfUsers = _dbCtx.Users_Projects_ProjectRoles.Where(u => (u.AccountID == CurrentAccount.AccountID && u.ProjectID == CurrentProject.ProjectID)).Select(s => s.User).ToList();
            cbRequester.ItemsSource = listOfUsers;
        }
        /// <summary>
        /// Close current window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Save story and add to icebox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            //  check if all fields are filled then save
            if(cbStoryType.SelectedIndex != -1 &&
                cbStoryPriority.SelectedIndex != -1 &&
                cbStoryStatus.SelectedIndex != -1 &&
                cbOwner.SelectedIndex != -1 &&
                cbRequester.SelectedIndex != -1 &&
                txtStoryDescription.Text.Length != 0 &&
                txtPoints.Text.Length != 0)
            {
                using (var trans = _dbCtx.Database.BeginTransaction())
                {
                    try
                    {

                        if(Convert.ToInt32(txtPoints.Text) > 0)
                        {
                            //  Save user story to project icebox
                            Icebox box = _dbCtx.Iceboxes.Where(i => i.ProjectID == CurrentProject.ProjectID).FirstOrDefault();
                           // _dbCtx.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[UserStory] ON");

                            box.UserStories.Add(new UserStory
                            {
                                Description = txtStoryDescription.Text,
                                Points = Convert.ToInt32(txtPoints.Text),
                                StoryTypeID = (cbStoryType.SelectedItem as StoryType).StoryTypeID,
                                PriorityID = (cbStoryPriority.SelectedItem as Priority).PriorityID,
                                StatusID = (cbStoryStatus.SelectedItem as Status).StatusID,
                                OwnerUserID = (cbOwner.SelectedItem as User).UserID,
                                RequesterUserID = (cbRequester.SelectedItem as User).UserID
                            });                            

                            //  Save to db
                            _dbCtx.SaveChanges();
                          //  _dbCtx.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[UserStory] OFF");
                            trans.Commit();
                            MessageBox.Show("Story Created!");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Story points cannot be zero!");
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.Message);
                        return;
                    }
                }

            }
            else
            {
                MessageBox.Show("All fields are required!");
                return;
            }
        }
    }
}