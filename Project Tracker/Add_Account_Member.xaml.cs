﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TrackerDAL;
namespace ProjectTracker
{
    /// <summary>
    /// Interaction logic for Add_Account_Member.xaml
    /// </summary>
    public partial class Add_Account_Member : Window
    {
        private readonly TrackerContext _dbCtx;
        private User CurrentUser;
        List<AccountRole> AccountRoles;
        public Add_Account_Member(User user)
        {
            _dbCtx = new TrackerContext();
            CurrentUser = user;
            AccountRoles = _dbCtx.AccountRoles.ToList();

            InitializeComponent();
        }

        private void CbRole_Loaded(object sender, RoutedEventArgs e)
        {
           //   Select all roles where role isnt owner, owner will be set in another window
            cbRole.ItemsSource = AccountRoles.Where(r => r.AccountRoleName != "Owner" ).Select(s=>s).ToList();
        }

        private void BtnAddAccountMember_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContextTransaction = _dbCtx.Database.BeginTransaction())
            {
                try
                {
                    Users_Accounts users_Accounts = new Users_Accounts();
                    Account curAcc = _dbCtx.Users_Accounts.Where(u => (u.UserID == CurrentUser.UserID && u.AccountRoleID == (int)AccountRole.Role.OWNER)).FirstOrDefault().Account;
                    if (txtAddByName.Text.Length != 0 || txtAddByEmail.Text.Length != 0)
                    {
                        //  check if fields arent empty
                        if (txtAddByName.Text.Length != 0)
                        {

                            //  check if user exists and then add
                            var user = _dbCtx.Users.Where(u => (u.Firstname + " " + u.Lastname).Contains(txtAddByName.Text)).FirstOrDefault();
                            if (user != null)
                            {
                                //check if user is already associated with the current account
                               
                                var userInAcc = _dbCtx.Users_Accounts.Where(u => (u.UserID == user.UserID && u.AccountID == curAcc.AccountID)).FirstOrDefault();
                                if (userInAcc != null)
                                {
                                    MessageBox.Show("User is already associated with this account!");
                                    return;
                                }

                                //  check if cb account role is selected
                                if (cbRole.SelectedIndex > -1)
                                {
                                    //  Add new user to current user's account
                                    users_Accounts.UserID = user.UserID;
                                    users_Accounts.AccountID = curAcc.AccountID;
                                    users_Accounts.AccountRoleID = (cbRole.SelectedItem as AccountRole).AccountRoleID;

                                    //  add to db
                                    _dbCtx.Users_Accounts.Add(users_Accounts);
                                    _dbCtx.SaveChanges();

                                    //  commit transaction
                                    dbContextTransaction.Commit();
                                    MessageBox.Show("User added to account!");
                                    return;

                                }
                                else
                                {
                                    MessageBox.Show("A role must be selected!");
                                    return;
                                }
                            }
                            else
                            {
                                MessageBox.Show("User not found! ");
                                return;
                            }
                        }

                        if (txtAddByEmail.Text.Length != 0)
                        {
                            //  check if user exists and then add
                            var user = _dbCtx.Users.Where(u => u.Email == txtAddByEmail.Text).FirstOrDefault();
                            if (user != null)
                            {
                                //check if user is already associated with the current account
                                var userInAcc = _dbCtx.Users_Accounts.Where(u => (u.UserID == user.UserID && u.AccountID == curAcc.AccountID)).FirstOrDefault();
                                if (userInAcc != null)
                                {
                                    MessageBox.Show("User is already associated with this account!");
                                    return;
                                }
                                //  check if cb account role is selected
                                if (cbRole.SelectedIndex > -1)
                                {
                                    //  Add new user to current user's account
                                    users_Accounts.UserID = user.UserID;
                                    users_Accounts.AccountID = curAcc.AccountID;

                                    users_Accounts.AccountRoleID = (cbRole.SelectedItem as AccountRole).AccountRoleID;



                                    //  add to db
                                    _dbCtx.Users_Accounts.Add(users_Accounts);
                                    _dbCtx.SaveChanges();
                                    dbContextTransaction.Commit();
                                    MessageBox.Show("User added to account!");
                                    return;

                                }
                                else
                                {
                                    MessageBox.Show("A role must be selected!");
                                    return;
                                }
                            }
                            else
                            {
                                MessageBox.Show("User not found! ");
                                return;
                            }

                        }
                    }
                    else
                    {
                        MessageBox.Show("You must enter user name OR user email");
                        return;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    dbContextTransaction.Rollback();
                    return;
                }
            }
                     
        }
    }
}
