﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectTracker
{
    class Resource : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        private string Name_;

        public string Name
        {
            get { return Name_; }
            set
            {
                Name_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
            }
        }
        private string Email_;

        public string Email
        {
            get { return Email_; }
            set
            {
                Email_ = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Email"));
            }
        }

        //private string type_;

        //public string Type
        //{
        //    get { return type_; }
        //    set
        //    {
        //        type_ = value;
        //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Type"));
        //    }
        //}

        //private string initials_;
        //public string Initials
        //{
        //    get { return initials_; }
        //    set
        //    {
        //        initials_ = value;
        //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Initials"));
        //    }
        //}


        //private string maxUnits_;
        //public string MaxUnits
        //{
        //    get { return maxUnits_; }
        //    set
        //    {
        //        maxUnits_ = value;
        //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("MaxUnits"));
        //    }
        //}

        //private string stdRates_;
        //public string StandardRates
        //{
        //    get { return stdRates_; }
        //    set
        //    {
        //        stdRates_ = value;
        //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("StandardRates"));
        //    }
        //}

        //private string OTRates_;
        //public string OvertimeRates
        //{
        //    get { return OTRates_; }
        //    set
        //    {
        //        OTRates_ = value;
        //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("OvertimeRates"));
        //    }
        //}

        //private string costPerUse_;
        //public string CostPerUse
        //{
        //    get { return costPerUse_; }
        //    set
        //    {
        //        costPerUse_ = value;
        //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CostPerUse"));
        //    }
        //}

        //private string accrueAt_;
        //public string AccrueAt
        //{
        //    get { return accrueAt_; }
        //    set
        //    {
        //        accrueAt_ = value;
        //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AccrueAt"));
        //    }
        //}

        //private string baseCalendar_;
        //public string BaseCalendar
        //{
        //    get { return baseCalendar_; }
        //    set
        //    {
        //        baseCalendar_ = value;
        //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("BaseCalendar"));
        //    }
        //}
    }
}
