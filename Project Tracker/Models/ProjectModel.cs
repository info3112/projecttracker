﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectTracker.Models
{
     public class ProjectModel
    {
        public int ProjectID { get; set; }
     
        public string ProjectName { get; set; }

        public bool AccessRestricted { get; set; }
        public override string ToString()
        {
            return ProjectName;
        }
    }
}
