﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TrackerDAL;
using ProjectTracker.Models;

namespace ProjectTracker
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        TrackerContext _dbCtx;
        User CurrentUser;
        Account CurrentAccount;
        Account CurrentSelectedAccount;
        Project CurrentProject;       
        List<Account> accounts = new List<Account>();
        ObservableCollection<ProjectModel> Projects;
        public Dashboard(User user)
        {           
            InitializeComponent();

            //  set current user 
            CurrentUser = user;
            CurrentAccount = CurrentUser.Users_Accounts.Where(u => u.AccountRoleID == (int)AccountRole.Role.OWNER).FirstOrDefault().Account;
            CurrentSelectedAccount = CurrentAccount;
            _dbCtx = new TrackerContext();
            Projects = new ObservableCollection<ProjectModel>();
            menuLogon.Header = "Signed in as: " +CurrentUser.Firstname +" "+ CurrentUser.Lastname;
            // lblUserName.Content = "User: " + CurrentUser.Firstname + " " + CurrentUser.Lastname;
        }
      
        private void MenuItem_ViewAllResources_Click(object sender, RoutedEventArgs e)
        {
            ShowResources showResources = new ShowResources(CurrentSelectedAccount);
            showResources.ShowDialog();
        }

        private void MenuItem_CreateNewProject_Click(object sender, RoutedEventArgs e)
        {
            CreateNewProject createNewProject = new CreateNewProject(CurrentUser);
            createNewProject.ShowDialog();
        }

        //  Add a new account member by email or full name
        private void MenuItem_Click_Add_Account_Member(object sender, RoutedEventArgs e)
        {

            Add_Account_Member aam = new Add_Account_Member(CurrentUser);
            aam.ShowDialog();
        }
      
      
        /// <summary>
        /// Fill projects list 
        /// </summary>
        private void GetProjects()
        {

            try
            {
               

                //  get all projects on current account
                if (Projects.Count > 0) Projects.Clear();
                var list = _dbCtx.Users_Projects_ProjectRoles
                    .Where(u => (u.AccountID == CurrentSelectedAccount.AccountID && u.UserID == CurrentUser.UserID))
                    .Select(s => s.Project)
                    .Select(p => new ProjectModel { ProjectID = p.ProjectID, ProjectName = p.ProjectName, AccessRestricted = p.AccessRestricted })
                    .ToList();
                foreach (var item in list)
                {
                    Projects.Add(item);
                }
                Projects.CollectionChanged += Projects_CollectionChanged;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }


        }

        private void Projects_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            dgAccountProjects.Items.Refresh();
        }

        /// <summary>
        /// Refresh Data Grid list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnProjectRefresh_Click(object sender, RoutedEventArgs e)
        {
            GetProjects();
            dgAccountProjects.Items.Refresh();

        }

        /// <summary>
        /// Open Selected Project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnProjectOpen_Click(object sender, RoutedEventArgs e)
        {
            if(CurrentProject != null)
            {
                //  Open project dashboard
                ProjectOverview po = new ProjectOverview(CurrentProject, CurrentUser, CurrentSelectedAccount);
                po.ShowDialog();
            }
        }

        /// <summary>
        /// Set current project according to selection in data grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgAccountProjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (dgAccountProjects.SelectedIndex != -1)
                {
                    var p = (dgAccountProjects.SelectedItem as ProjectModel);
                    if (p != null)
                    {
                        CurrentProject = _dbCtx.Projects.Where(prj => prj.ProjectID == p.ProjectID).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        /// <summary>
        /// Load accounts in combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbAccounts_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                accounts = new List<Account>();
                //  find all accounts cur user is part of
                //  show all where cur user has permissions to view
                var listOfAccountIDs = _dbCtx.Users_Accounts.Where(u => u.UserID == CurrentUser.UserID).Select(s => s.AccountID).ToList();

                foreach (int id in listOfAccountIDs)
                {
                    accounts.Add(_dbCtx.Accounts.Where(a => a.AccountID == id).Select(s => s).FirstOrDefault());
                }
                cbAccounts.ItemsSource = accounts.Distinct();
                if(cbAccounts.Items.Count>0) cbAccounts.SelectedIndex = 0;

                GetProjects();
                dgAccountProjects.DataContext = Projects;
                dgAccountProjects.CanUserAddRows = false;
                dgAccountProjects.CanUserDeleteRows = false;
                dgAccountProjects.IsReadOnly = true;
                dgAccountProjects.AutoGenerateColumns = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }           
        }

        /// <summary>
        /// Logoff logic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Logout_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow window = new LoginWindow();
            window.Show();
            this.Close();      
        }

        /// <summary>
        /// Add an existing user to project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_AddUserToProject_Click(object sender, RoutedEventArgs e)
        {
            if(CurrentProject != null)
            {
                AddUserToProject addUser = new AddUserToProject(CurrentSelectedAccount,CurrentUser,CurrentProject);
                addUser.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please select a project from the List");
                return;
            }
        }

        private void CbAccounts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(cbAccounts.SelectedIndex != -1)
            {
                //  get account
                if (cbAccounts.SelectedIndex != -1)
                {
                    if (cbAccounts.SelectedItem is Account)
                    {
                        CurrentSelectedAccount = cbAccounts.SelectedItem as Account;

                        //  Refresh datagrid list
                        GetProjects();
                    }
                }
            }
        }

        private void MenuItem_File_Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Environment.Exit(0);
        }

        private void MenuItem_File_About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Project Tracker");
        }
    }
}
