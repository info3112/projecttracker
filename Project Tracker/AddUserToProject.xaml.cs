﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TrackerDAL;

namespace ProjectTracker
{
    /// <summary>
    /// Interaction logic for AddUserToProject.xaml
    /// </summary>
    public partial class AddUserToProject : Window
    {
        Account CurrentAccount;
        User CurrentUser;
        Project SelectedProject;
         TrackerContext _dbCtx;
        public AddUserToProject(Account acc,User user,Project proj)
        {
            _dbCtx = new TrackerContext();
            CurrentUser = user;
            CurrentAccount = acc;
            SelectedProject = proj;
            InitializeComponent();

            //  Set project name
            lblSelectedProject.Content = proj.ProjectName;
           
        }

        /// <summary>
        /// Load users on current account
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbUsersInAccount_Loaded(object sender, RoutedEventArgs e)
        {
            //  only show users that havent been added to selected project
            var listOfUsers = _dbCtx.Users_Projects_ProjectRoles.Where(u => (u.AccountID == CurrentAccount.AccountID && u.ProjectID != SelectedProject.ProjectID)).Select(s => s.User).ToList();
            
            cbUsersInAccount.ItemsSource = listOfUsers;
          

        }

        /// <summary>
        /// Add user to project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAddUserToProject_Click(object sender, RoutedEventArgs e)
        {
            //  check if current user has privilege to add users to project
            var curUserProjRole = _dbCtx.Users_Projects_ProjectRoles.Where(u => (u.UserID == CurrentUser.UserID && u.ProjectID == SelectedProject.ProjectID)).FirstOrDefault().ProjectRoleID;
            if (curUserProjRole != 1)   // if user is not project owner
            {
                MessageBox.Show("Only project owner can add users.","Insufficient Permissions",MessageBoxButton.OK,MessageBoxImage.Error);
                return;
            }
            //  else add the user
           using (var trans = _dbCtx.Database.BeginTransaction())
            {            
                try
                {
                    if (cbUsersInAccount.SelectedIndex != -1 && cbProjectRole.SelectedIndex != -1)
                    {
                        if (cbUsersInAccount.SelectedItem is User)
                        {
                            User user = cbUsersInAccount.SelectedItem as User;
                            Users_Projects_ProjectRoles newUser = new Users_Projects_ProjectRoles();
                            newUser.UserID = user.UserID;
                            newUser.ProjectID = SelectedProject.ProjectID;
                            newUser.ProjectRoleID = (cbProjectRole.SelectedItem as ProjectRole).ProjectRoleID;
                            newUser.AccountID = CurrentAccount.AccountID;
                            _dbCtx.Users_Projects_ProjectRoles.Add(newUser);
                            _dbCtx.SaveChanges();
                            trans.Commit();
                        }
                    }
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    MessageBox.Show(ex.Message);
                    return;
                }
            }

        }

        /// <summary>
        /// Load project Roles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbProjectRole_Loaded(object sender, RoutedEventArgs e)
        {
            cbProjectRole.ItemsSource = _dbCtx.ProjectRoles.Where(r => r.ProjectRoleID > 1).Select(s => s).ToList();
        }
    }
}
