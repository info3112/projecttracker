﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TrackerDAL;
namespace ProjectTracker
{
    /// <summary>
    /// Interaction logic for ProjectOverview.xaml
    /// </summary>
    public partial class ProjectOverview : Window
    {
        private Project CurrentProject;
        private User CurrentUser;
        private Account CurrentAccount;
        ObservableCollection<UserStory> IceboxStories;
        ObservableCollection<UserStory> CurrentSprintStories;
        ObservableCollection<Sprint> Sprints;
        TrackerContext _dbCtx;
        UserStory CurrentStory;
        public ProjectOverview(Project project, User user,Account acc)
        {
            _dbCtx = new TrackerContext();
            InitializeComponent();            
            CurrentAccount = acc;
            CurrentProject = project;
            CurrentUser = user;
            this.Title = CurrentProject.ProjectName;
            lblUserName.Content = "User: " + CurrentUser.Firstname + " " + CurrentUser.Lastname;
            IceboxStories = new ObservableCollection<UserStory>();
            CurrentSprintStories = new ObservableCollection<UserStory>();
            Sprints = new ObservableCollection<Sprint>();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //  Set project labels
                lblProjectName.Content = CurrentProject.ProjectName;
                var owner = CurrentProject.Users_Projects_ProjectRoles.Where(u => (u.ProjectID == CurrentProject.ProjectID
                && u.ProjectRoleID == (int)ProjectRole.Role.OWNER)).FirstOrDefault().User;
                lblProjectOwner.Content = owner.Firstname + " " + owner.Lastname;
                tbProjectDescription.Text = CurrentProject.ProjectDescription;
                //  Set Sprint dates
                var today = DateTime.Today;
                dpStartDate.SelectedDate = today;
                dpEndDate.SelectedDate = today.AddDays(14);                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void BtnStoryNew_Click(object sender, RoutedEventArgs e)
        {
            CreateStory createStory = new CreateStory(CurrentUser,CurrentAccount,CurrentProject);
            createStory.ShowDialog();
        }

        /// <summary>
        /// Open selected story for editing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnStoryOpen_Click(object sender, RoutedEventArgs e)
        {
            if(lbIceboxStories.SelectedIndex != -1)
            {
                //  get story item
                var story = lbIceboxStories.SelectedItem as UserStory;
                EditStory editStory = new EditStory(CurrentAccount, CurrentProject, story);
                editStory.ShowDialog();
            }
           
        }

        /// <summary>
        /// Load stories into listbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbIceboxStories_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                IceboxStories.Clear();
                var exists = _dbCtx.UserStories.Any(u => u.Iceboxes.Any(i => i.ProjectID == CurrentProject.ProjectID));
                if (exists)
                {
                    _dbCtx.Iceboxes.Where(i => i.ProjectID == CurrentProject.ProjectID).FirstOrDefault().UserStories.ToList().ForEach(s => IceboxStories.Add(s));
                    IceboxStories.CollectionChanged += IceboxStories_CollectionChanged;
                    lbIceboxStories.ItemsSource = IceboxStories;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
        
        /// <summary>
        /// Update List box on collection change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IceboxStories_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            lbIceboxStories.ItemsSource = null;
            lbIceboxStories.ItemsSource = IceboxStories;
        }

        /// <summary>
        /// Set selection as Current user story
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbIceboxStories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lbIceboxStories.SelectedIndex != -1)
            {
                CurrentStory = new UserStory();
                CurrentStory = lbIceboxStories.SelectedItem as UserStory;
            }
        }

        /// <summary>
        /// Add sprint to project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAddSprint_Click(object sender, RoutedEventArgs e)
        {
          if(dpStartDate.SelectedDate != null && dpEndDate.SelectedDate != null)
            {
                using (var trans = _dbCtx.Database.BeginTransaction())
                {
                    try
                    {
                        //  add sprint to project
                        Sprint sprint = new Sprint()
                        {
                            StartDate = (DateTime)dpStartDate.SelectedDate.Value.Date,
                            EndDate = (DateTime)dpEndDate.SelectedDate.Value.Date,
                            IsDone = false,
                            ProjectID = CurrentProject.ProjectID,
                            DisplayOrder = Sprints != null ? Sprints.Count + 1 : 0                           
                        };
                        _dbCtx.Sprints.Add(sprint);
                        _dbCtx.SaveChanges();
                        trans.Commit();
                        MessageBox.Show("Sprint Added!");
                        //  update sprints list
                        Sprints.Clear();
                        _dbCtx.Sprints.Where(s => s.ProjectID == CurrentProject.ProjectID).ToList().ForEach(s => Sprints.Add(s));
                        
                    }
                    catch(Exception ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.Message);
                        return;
                    }
                   
                }

            }
            else
            {
                MessageBox.Show("Please select Start and End Dates.");
                return;
            }
        }

        /// <summary>
        /// load project sprints in listbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbSprints_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //get sprints and set to listbox
                Sprints = new ObservableCollection<Sprint>();
                var list = _dbCtx.Sprints.Where(s => s.ProjectID == CurrentProject.ProjectID).ToList();
                foreach (var item in list)
                {
                    Sprints.Add(item);
                }
                //  add collection change event handler
                Sprints.CollectionChanged += Sprints_CollectionChanged;

                lbSprints.ItemsSource = Sprints;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
        /// <summary>
        /// Refresh listbox on source change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sprints_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            lbSprints.ItemsSource = null;
            lbSprints.ItemsSource = Sprints;
            cbSprints.ItemsSource = null;
            cbSprints.ItemsSource = Sprints;
        }

        /// <summary>
        /// Show project sprints
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbSprints_Loaded(object sender, RoutedEventArgs e)
        {
            if(Sprints != null)
            {
                cbSprints.ItemsSource = null;
                cbSprints.ItemsSource = Sprints;
               
            }
        }

        /// <summary>
        /// Show stories in a sprint/backlog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbSprintStories_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cbSprints.SelectedIndex != -1)
                {
                    //get sprint from cb
                    var sprint = cbSprints.SelectedItem as Sprint;
                    CurrentSprintStories.Clear();
                    _dbCtx.Sprints.Where(s => s.SprintID == sprint.SprintID).FirstOrDefault().UserStories.ToList().ForEach(s => CurrentSprintStories.Add(s));
                    CurrentSprintStories.CollectionChanged += CurrentSprintStories_CollectionChanged;
                    lbSprintStories.ItemsSource = CurrentSprintStories;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

        }

        private void CurrentSprintStories_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            lbSprintStories.ItemsSource = null;
            lbSprintStories.ItemsSource = CurrentSprintStories;
        }

        /// <summary>
        /// Add user story from Icebox to sprint
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAddToSprint_Click(object sender, RoutedEventArgs e)
        {
            if(cbSprints.SelectedIndex != -1 && lbIceboxStories.SelectedIndex != -1)
            {
                using (var trans = _dbCtx.Database.BeginTransaction())
                {
                    try
                    {


                        //  get story and sprint
                        var story = lbIceboxStories.SelectedItem as UserStory;
                        var sprint = cbSprints.SelectedItem as Sprint;

                        //  add story to sprint/backlog and remove from icebox
                        sprint.UserStories.Add(story);
                        story.Iceboxes.Where(i => i.ProjectID == CurrentProject.ProjectID).FirstOrDefault().UserStories.Remove(story);
                        _dbCtx.SaveChanges();
                        trans.Commit();

                        //  Update Icebox list and sprint story list
                        IceboxStories.Clear();
                         _dbCtx.Iceboxes.Where(i => i.ProjectID == CurrentProject.ProjectID).FirstOrDefault().UserStories.ToList().ForEach(s=> IceboxStories.Add(s));
                        CurrentSprintStories.Clear();
                        _dbCtx.Sprints.Where(s => (s.SprintID== sprint.SprintID && s.ProjectID == CurrentProject.ProjectID)).FirstOrDefault().UserStories.ToList().ForEach(s=> CurrentSprintStories.Add(s));
                    }
                    catch(Exception ex)
                    {
                        trans.Rollback();
                        MessageBox.Show(ex.Message);
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Select a story and sprint!");
                return;
            }

        }

        /// <summary>
        /// Load sprint stories for selected sprint
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbSprints_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cbSprints.SelectedIndex != -1)
                {
                    var sprint = cbSprints.SelectedItem as Sprint;
                    //  update sprint stories collection with current sprint stories
                    CurrentSprintStories.Clear();
                    _dbCtx.Sprints.Where(s => s.SprintID == sprint.SprintID).FirstOrDefault().UserStories.ToList().ForEach(s => CurrentSprintStories.Add(s));
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
           
        }

        /// <summary>
        /// Refresh ice box stories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Refresh_Click(object sender, RoutedEventArgs e)
        {
            //  get stories from db and add to collection
            try
            {
                IceboxStories.Clear();
                var exists = _dbCtx.UserStories.Any(u => u.Iceboxes.Any(i => i.ProjectID == CurrentProject.ProjectID));
                if (exists)
                {
                    _dbCtx.Iceboxes.Where(i => i.ProjectID == CurrentProject.ProjectID).FirstOrDefault().UserStories.ToList().ForEach(s => IceboxStories.Add(s));
                    IceboxStories.CollectionChanged += IceboxStories_CollectionChanged;
                    lbIceboxStories.ItemsSource = null;
                    lbIceboxStories.ItemsSource = IceboxStories;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
    }
}
