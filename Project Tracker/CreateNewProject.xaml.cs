﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TrackerDAL;
namespace ProjectTracker
{
    /// <summary>
    /// Interaction logic for CreateNewProject.xaml
    /// </summary>
    public partial class CreateNewProject : Window
    {
        private readonly TrackerContext _dbCtx;
        User CurrentUser;
        List<Account> accounts;
        
        public CreateNewProject(User user)
        {
            _dbCtx = new TrackerContext();

            CurrentUser = user;
            InitializeComponent();
            rbPrivate.IsChecked = true;
        }

        private void CbAccountName_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //  find all accounts cur user is part of
                //  show all where cur user has permissions of admin or higher
                var listOfAccountIDs = _dbCtx.Users_Accounts.Where(u => (u.UserID == CurrentUser.UserID && u.AccountRoleID < (int)AccountRole.Role.MEMBER)).Select(s => s.AccountID).ToList();
                accounts = new List<Account>();
                foreach (int id in listOfAccountIDs)
                {
                    accounts.Add(_dbCtx.Accounts.Where(a => a.AccountID == id).Select(s=>s).FirstOrDefault());
                }
                cbAccountName.ItemsSource = accounts.Distinct();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
           
            
        }

        private void Button_Click_Cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_Create(object sender, RoutedEventArgs e)
        {
            using (var dbcontextTransaction = _dbCtx.Database.BeginTransaction())
            {
                try
                {
                    //  get proj name
                    if (txtProjectName.Text.Length != 0)
                    {
                        //  get acc from combobox
                        if (cbAccountName.SelectedIndex > -1)
                        {

                            Project project = new Project();
                            //  set private or public and add to db
                            if ((bool)rbPublic.IsChecked)
                                project.AccessRestricted = false;
                            else
                                project.AccessRestricted = true;

                            project.ProjectName = txtProjectName.Text;

                            //  add and get proj with id
                            project = _dbCtx.Projects.Add(project);
                            _dbCtx.SaveChanges();

                            //  get selected account to add to
                            Account acc = (cbAccountName.SelectedItem as Account);


                            //  Add project info to user_projects_projectroles tables
                            _dbCtx.Users_Projects_ProjectRoles.Add(
                            new Users_Projects_ProjectRoles
                            {
                                UserID = CurrentUser.UserID,
                                ProjectID = project.ProjectID,
                                ProjectRoleID = (int)ProjectRole.Role.OWNER,
                                AccountID = acc.AccountID
                            });

                            if (_dbCtx.SaveChanges() == 1)
                            {
                                dbcontextTransaction.Commit();
                              
                                using (var transaction = _dbCtx.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        //  Create new Icebox for this project
                                        Icebox icebox = new Icebox();
                                        icebox.ProjectID = project.ProjectID;
                                        _dbCtx.Iceboxes.Add(icebox);
                                        _dbCtx.SaveChanges();
                                        transaction.Commit();
                                    }
                                    catch(Exception ex)
                                    {
                                        MessageBox.Show(ex.Message);
                                        return;
                                    }
                                  
                                }
                                  
                                
                                MessageBox.Show("Project Added!");
                                this.Close();
                                return;
                            }
                            else
                            {
                                MessageBox.Show("Project not Added.");
                                return;
                            }

                        }
                        else
                        {
                            MessageBox.Show("Please select an account to add the project to!");
                            return;
                        }
                    }
                    else { MessageBox.Show("Project Name cannot be empty!"); return; }



                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    dbcontextTransaction.Rollback();                    
                    return;
                }
            }
        }
    }
}
