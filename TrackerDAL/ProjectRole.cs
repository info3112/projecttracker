namespace TrackerDAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProjectRole
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProjectRole()
        {
            Users_Projects_ProjectRoles = new HashSet<Users_Projects_ProjectRoles>();
        }

        public int ProjectRoleID { get; set; }

        [Required]
        [StringLength(255)]
        public string ProjectRoleDesc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Users_Projects_ProjectRoles> Users_Projects_ProjectRoles { get; set; }
        public enum Role { OWNER = 1, MEMBER = 2, VIEWER = 3 }
        public override string ToString()
        {
            return ProjectRoleDesc;
        }
    }
}
