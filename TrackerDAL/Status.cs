namespace TrackerDAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Status
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Status()
        {
            UserStories = new HashSet<UserStory>();
        }

        public int StatusID { get; set; }

        [Required]
        [StringLength(50)]
        public string StatusDescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserStory> UserStories { get; set; }
        public enum StoryStatus { COMPLETED = 1, ON_HOLD = 2, STARTED = 3, UNFINISHED = 4, REJECTED = 5, DELIVERED = 6, UNSTARTED = 7, ACCEPTED = 8 }
        public override string ToString()
        {
            return StatusDescription;
        }
    }
}
