namespace TrackerDAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Project
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Project()
        {
            Iceboxes = new HashSet<Icebox>();
            Sprints = new HashSet<Sprint>();
            Users_Projects_ProjectRoles = new HashSet<Users_Projects_ProjectRoles>();
        }

        public int ProjectID { get; set; }

        [Required]
        [StringLength(255)]
        public string ProjectName { get; set; }

        public bool AccessRestricted { get; set; }

        public string ProjectDescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Icebox> Iceboxes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sprint> Sprints { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Users_Projects_ProjectRoles> Users_Projects_ProjectRoles { get; set; }
    }
}
