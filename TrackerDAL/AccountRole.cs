namespace TrackerDAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AccountRole
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AccountRole()
        {
            Users_Accounts = new HashSet<Users_Accounts>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountRoleID { get; set; }

        [Required]
        [StringLength(50)]
        public string AccountRoleName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Users_Accounts> Users_Accounts { get; set; }
        public enum Role { OWNER = 1, ADMIN = 2, PROJECT_CREATOR = 3, MEMBER = 4 }
        public override string ToString()
        {
            return AccountRoleName;
        }
    }
}
