namespace TrackerDAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StoryType")]
    public partial class StoryType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public StoryType()
        {
            UserStories = new HashSet<UserStory>();
        }

        public int StoryTypeID { get; set; }

        [Required]
        [StringLength(50)]
        public string StoryTypeDesc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserStory> UserStories { get; set; }
        public enum Type { FEATURE = 1, BUG = 2, CHORE = 3 }
        public override string ToString()
        {
            return StoryTypeDesc;
        }
    }
}
