namespace TrackerDAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Account
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Account()
        {
            Users_Accounts = new HashSet<Users_Accounts>();
            Users_Projects_ProjectRoles = new HashSet<Users_Projects_ProjectRoles>();
        }

        public int AccountID { get; set; }

        [Required]
        [StringLength(255)]
        public string AccountName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Users_Accounts> Users_Accounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Users_Projects_ProjectRoles> Users_Projects_ProjectRoles { get; set; }
        public override string ToString()
        {
            return AccountID + " " + AccountName;
        }
    }
}
