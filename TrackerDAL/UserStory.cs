namespace TrackerDAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserStory")]
    public partial class UserStory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserStory()
        {
            Iceboxes = new HashSet<Icebox>();
            Sprints = new HashSet<Sprint>();
            Comments = new HashSet<Comment>();
        }

        public int UserStoryID { get; set; }

        [Required]
        public string Description { get; set; }

        public int OwnerUserID { get; set; }

        public int StoryTypeID { get; set; }

        public int RequesterUserID { get; set; }

        public int StatusID { get; set; }

        public int Points { get; set; }

        public int PriorityID { get; set; }

        public virtual Priority Priority { get; set; }

        public virtual Status Status { get; set; }

        public virtual StoryType StoryType { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Icebox> Iceboxes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sprint> Sprints { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }
        public override string ToString()
        {
            return "#" + UserStoryID + " : " + Description;
        }
    }
}
