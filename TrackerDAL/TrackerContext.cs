namespace TrackerDAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TrackerContext : DbContext
    {
        public TrackerContext()
            : base("name=TrackerContext")
        {
        }

        public virtual DbSet<AccountRole> AccountRoles { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Icebox> Iceboxes { get; set; }
        public virtual DbSet<Priority> Priorities { get; set; }
        public virtual DbSet<ProjectRole> ProjectRoles { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Sprint> Sprints { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<StoryType> StoryTypes { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserStory> UserStories { get; set; }
        public virtual DbSet<Users_Accounts> Users_Accounts { get; set; }
        public virtual DbSet<Users_Projects_ProjectRoles> Users_Projects_ProjectRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountRole>()
                .HasMany(e => e.Users_Accounts)
                .WithRequired(e => e.AccountRole)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.AccountName)
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Users_Accounts)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Users_Projects_ProjectRoles)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Comment>()
                .Property(e => e.Comment1)
                .IsUnicode(false);

            modelBuilder.Entity<Comment>()
                .HasMany(e => e.UserStories)
                .WithMany(e => e.Comments)
                .Map(m => m.ToTable("UserStory_Comments").MapLeftKey("CommentID").MapRightKey("UserStoryID"));

            modelBuilder.Entity<Icebox>()
                .HasMany(e => e.UserStories)
                .WithMany(e => e.Iceboxes)
                .Map(m => m.ToTable("Icebox_UserStory").MapLeftKey("IceboxID").MapRightKey("UserStoryID"));

            modelBuilder.Entity<Priority>()
                .Property(e => e.PriorityDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Priority>()
                .HasMany(e => e.UserStories)
                .WithRequired(e => e.Priority)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProjectRole>()
                .Property(e => e.ProjectRoleDesc)
                .IsUnicode(false);

            modelBuilder.Entity<ProjectRole>()
                .HasMany(e => e.Users_Projects_ProjectRoles)
                .WithRequired(e => e.ProjectRole)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Project>()
                .Property(e => e.ProjectDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.Iceboxes)
                .WithRequired(e => e.Project)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.Sprints)
                .WithRequired(e => e.Project)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.Users_Projects_ProjectRoles)
                .WithRequired(e => e.Project)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sprint>()
                .HasMany(e => e.UserStories)
                .WithMany(e => e.Sprints)
                .Map(m => m.ToTable("Sprint_UserStory").MapLeftKey("SprintID").MapRightKey("UserStoryID"));

            modelBuilder.Entity<Status>()
                .Property(e => e.StatusDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Status>()
                .HasMany(e => e.UserStories)
                .WithRequired(e => e.Status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StoryType>()
                .Property(e => e.StoryTypeDesc)
                .IsUnicode(false);

            modelBuilder.Entity<StoryType>()
                .HasMany(e => e.UserStories)
                .WithRequired(e => e.StoryType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Task>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Tasks)
                .Map(m => m.ToTable("Users_Tasks").MapLeftKey("TaskID").MapRightKey("UserID"));

            modelBuilder.Entity<User>()
                .Property(e => e.Firstname)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Lastname)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Users_Accounts)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Users_Projects_ProjectRoles)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserStories)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.OwnerUserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserStories1)
                .WithRequired(e => e.User1)
                .HasForeignKey(e => e.RequesterUserID)
                .WillCascadeOnDelete(false);
        }
    }
}
